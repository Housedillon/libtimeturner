//
//  FileFixer.swift
//  FixEncodedDate
//
//  Created by William Dillon on 1/9/21.
//

import Foundation

public func fixFileDate(url: URL, date: Date) throws {
    try FileManager().setAttributes(
            [FileAttributeKey.creationDate: date,
             FileAttributeKey.modificationDate: date],
            ofItemAtPath: url.path
    )
}

public func getFileDate(url: URL) throws -> Date? {
    let attributes = try FileManager().attributesOfItem(atPath: url.path)

    return attributes[FileAttributeKey.creationDate] as? Date
}
