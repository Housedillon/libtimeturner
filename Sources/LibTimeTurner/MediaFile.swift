//
//  MediaFile.swift
//  FixEncodedDate
//
//  Created by William Dillon on 1/9/21.
//

import Foundation

private let fileManager = FileManager()

public struct MediaFile {
    static func files(fromUrl url: URL) throws -> [MediaFile] {
        var isDir: ObjCBool = false
        if fileManager.fileExists(atPath: url.path, isDirectory: &isDir) {
            if isDir.boolValue == true {
                var retval: [MediaFile] = []
                for path in try fileManager.subpathsOfDirectory(atPath: url.path) {
                    let pathUrl = url.appendingPathComponent(path)
                    var isDir: ObjCBool = false
                    if fileManager.fileExists(atPath: pathUrl.path, isDirectory: &isDir) {
                        if isDir.boolValue == false {
                            retval.append(MediaFile(url: pathUrl))
                        } else {
                            retval.append(contentsOf: try MediaFile.files(fromUrl: pathUrl))
                        }
                    }
                }
                return retval
            } else {
                return [MediaFile(url: url)]
            }
        } else {
            return []
        }
    }

    public enum MediaFileState {
        case identified
        case metadata
        case fixed
    }

    public enum MediaFileError: Error {
        case noMetaData
        case noDate
    }

    public let url: URL
    public var state: MediaFileState
    public let wraper = try! FFProbeWrapper()
    public var metadata: FFProbeMetadata?

    public init(url: URL) {
        self.url = url
        self.state = .identified
    }

    public mutating func getMetadata() throws {
        metadata = try wraper.getMetadata(url: url)
        state = .metadata
    }

    public mutating func fixCreationTime() throws {
        guard let metadata = metadata else {
            throw MediaFileError.noMetaData
        }

        guard let date = metadata.getDate() else {
            throw MediaFileError.noDate
        }

//        if let originalDate = try getFileDate(url: url) {
//            print("File \(url.path) originally \(originalDate) becoming \(date)")
//        }
        try fixFileDate(url: url, date: date)
    }
}
