//
//  FFProbeWrapper.swift
//  FixEncodedDate
//
//  Created by William Dillon on 1/9/21.
//

import Foundation

public struct FFProbeMetadata: Codable {
    public func getDate() -> Date? {
        guard let string = format.tags["creation_time"] else {
            return nil
        }

        // The apple dateformatter is dumb, and can't really handle

        let dateFormatter = ISO8601DateFormatter()
        dateFormatter.formatOptions = [.withInternetDateTime, .withFractionalSeconds]
        return dateFormatter.date(from: string)
    }

    public enum CodecType: String, Codable {
        case video
        case audio
        case data
    }

    public struct Format: Codable {
        let tags: [String: String]
    }

    public struct Stream: Codable {
        let index: Int
        let codecType: FFProbeMetadata.CodecType
        let tags: [String: String]?

        enum CodingKeys: String, CodingKey {
            case index = "index"
            case codecType = "codec_type"
            case tags = "tags"
        }
    }

    public let streams: [FFProbeMetadata.Stream]
    public let format: FFProbeMetadata.Format
}

public enum FFProbeError: Error {
    case executableNotFound
    case fileNotFound
    case executionError(String?)
}

public struct FFProbeWrapper {
    let executablePath: URL

    init(bundle inputBundle: Bundle? = nil) throws {

        // Are we in the XCtest context?
        let bundle = inputBundle ?? Bundle.main

        #if os(OSX)
        let epath = bundle.path(forResource: "ffprobe", ofType: nil)
        #else
        let epath = "ffprobe"
        #endif

        guard epath != nil else { throw FFProbeError.executableNotFound }

        executablePath = URL(fileURLWithPath: epath!)
    }

    func getMetadata(url: URL) throws -> FFProbeMetadata {
        // Do some quick sanity checking to make sure this is likely to succeed
        let fileManager = FileManager()

        guard fileManager.fileExists(atPath: url.path) else {
            throw FFProbeError.fileNotFound
        }

        guard fileManager.fileExists(atPath: executablePath.path) else {
            throw FFProbeError.executableNotFound
        }

        let process = Process()
        process.executableURL = executablePath
        process.arguments = [
            "-v", "quiet",
            url.path,
            "-print_format", "json",
            "-show_entries", "stream=index,codec_type:stream_tags=creation_time:format_tags=creation_time"
        ]

        let outputPipe = Pipe()
        let errorPipe = Pipe()

        process.standardOutput = outputPipe
        process.standardError  = errorPipe

        try process.run()

        let outputData = outputPipe.fileHandleForReading.readDataToEndOfFile()
        let errorData  = errorPipe.fileHandleForReading.readDataToEndOfFile()

        process.waitUntilExit()

        guard process.terminationStatus == 0 else {
            throw FFProbeError.executionError(String(data: errorData, encoding: .utf8))
        }

        return try JSONDecoder().decode(FFProbeMetadata.self, from: outputData)
    }
}
