// swift-tools-version:5.3
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "LibTimeTurner",
    platforms: [.macOS(.v10_13)],
    products: [
        .library(
            name: "LibTimeTurner",
            targets: ["LibTimeTurner"])
    ],
    dependencies: [
    ],
    targets: [
        .target(
            name: "LibTimeTurner",
            dependencies: []),
        .testTarget(
            name: "LibTimeTurnerTests",
            dependencies: ["LibTimeTurner"],
            resources: [.process("Resources")])
    ]
)
