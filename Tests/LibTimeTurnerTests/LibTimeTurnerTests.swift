import XCTest
@testable import LibTimeTurner

final class LibTimeTurnerTests: XCTestCase {
    func testDateFixer() throws {
        let fileManager = FileManager()
        let now = Date()

        let testBundle = Bundle.module

        let wrapper = try FFProbeWrapper.init(bundle: testBundle)

        let tests = [
            "KOEF4063.MOV": "2020-01-19T23:15:20.000000Z",
            "IMG_E8960.MOV": "2020-02-28T02:00:29.000000Z"
        ]

        for (file, _) in tests {
            if let string = testBundle.path(forResource: file, ofType: nil) {
                let url = try fileManager
                    .url(
                        for: FileManager.SearchPathDirectory.cachesDirectory,
                        in: FileManager.SearchPathDomainMask.userDomainMask,
                        appropriateFor: nil, create: true)
                    .appendingPathComponent(file)

                // Copy the file to a temporary location so we can mutate it
                // These files may exist if this is re-run after an issue
                try? FileManager().copyItem(
                    at: URL(fileURLWithPath: string),
                    to: url
                )

                // Set the file date to now(ish) to make sure it wasn't just correctly set before
                try fixFileDate(url: url, date: now)

                // Check the creation date
                if let fileDate = try getFileDate(url: url) {
                    XCTAssertEqual(now, fileDate)
                } else {
                    XCTAssert(false, "Unable to get file date")
                }

                // Set the file date to the desired time
                let metadata = try wrapper.getMetadata(url: url)
                guard let date = metadata.getDate() else {
                    XCTAssert(false, "Unable to get date from metadata")
                    continue
                }
                try fixFileDate(url: url, date: date)
                // Check it again
                if let fileDate = try getFileDate(url: url) {
                    XCTAssertEqual(date, fileDate)
                } else {
                    XCTAssert(false, "Unable to get file date")
                }

                // Remove the temporary file
                try fileManager.removeItem(at: url)
            }
        }
    }

    func testFFProbe() throws {
        let testBundle = Bundle.module

        let wrapper = try FFProbeWrapper.init(bundle: testBundle)

        let tests = [
            "KOEF4063.MOV": "2020-01-19T23:15:20.000000Z",
            "IMG_E8960.MOV": "2020-02-28T02:00:29.000000Z"
        ]

        for (file, date) in tests {
            if let string = testBundle.path(forResource: file, ofType: nil) {
                let url = URL(fileURLWithPath: string)
                let metadata = try wrapper.getMetadata(url: url)

                XCTAssertEqual(metadata.format.tags["creation_time"]!, date)
            } else {
                XCTAssert(false, "Unable to load test resource")
            }
        }
    }

    func testMetadataCodable() throws {
        let metadata = try JSONDecoder().decode(FFProbeMetadata.self, from: metadataJson.data(using: .utf8)!)

        XCTAssertEqual(metadata.streams[0].index, 0)
        XCTAssertEqual(metadata.streams[0].codecType, .video)
        XCTAssertEqual(metadata.streams[0].tags, ["creation_time": "2020-12-10T16:32:38.000000Z"])
        XCTAssertEqual(metadata.streams[1].index, 1)
        XCTAssertEqual(metadata.streams[1].codecType, .audio)
        XCTAssertEqual(metadata.streams[1].tags, ["creation_time": "2020-12-10T16:32:38.000000Z"])
        XCTAssertEqual(metadata.streams[2].index, 2)
        XCTAssertEqual(metadata.streams[2].codecType, .data)
        XCTAssertEqual(metadata.streams[2].tags, ["creation_time": "2020-12-10T16:32:38.000000Z"])
        XCTAssertEqual(metadata.streams[3].index, 3)
        XCTAssertEqual(metadata.streams[3].codecType, .data)
        XCTAssertEqual(metadata.streams[3].tags, ["creation_time": "2020-12-10T16:32:38.000000Z"])
        XCTAssertEqual(metadata.streams[4].index, 4)
        XCTAssertEqual(metadata.streams[4].codecType, .data)
        XCTAssertEqual(metadata.streams[4].tags, ["creation_time": "2020-12-10T16:32:38.000000Z"])
        XCTAssertEqual(metadata.format.tags, ["creation_time": "2020-12-10T16:32:38.000000Z"])
    }
}

let metadataJson = """
{
    "programs": [

    ],
    "streams": [
        {
            "index": 0,
            "codec_type": "video",
            "tags": {
                "creation_time": "2020-12-10T16:32:38.000000Z"
            }
        },
        {
            "index": 1,
            "codec_type": "audio",
            "tags": {
                "creation_time": "2020-12-10T16:32:38.000000Z"
            }
        },
        {
            "index": 2,
            "codec_type": "data",
            "tags": {
                "creation_time": "2020-12-10T16:32:38.000000Z"
            }
        },
        {
            "index": 3,
            "codec_type": "data",
            "tags": {
                "creation_time": "2020-12-10T16:32:38.000000Z"
            }
        },
        {
            "index": 4,
            "codec_type": "data",
            "tags": {
                "creation_time": "2020-12-10T16:32:38.000000Z"
            }
        }
    ],
    "format": {
        "tags": {
            "creation_time": "2020-12-10T16:32:38.000000Z"
        }
    }
}
"""
