import XCTest

#if !canImport(ObjectiveC)
public func allTests() -> [XCTestCaseEntry] {
    return [
        testCase(LibTimeTurnerTests.allTests)
    ]
}
#endif
