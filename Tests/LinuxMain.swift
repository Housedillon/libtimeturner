import XCTest

import LibTimeTurnerTests

var tests = [XCTestCaseEntry]()
tests += LibTimeTurnerTests.allTests()
XCTMain(tests)
